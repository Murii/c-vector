Usage:

```C
#include <stdio.h>

#include "vector.h"

int main(void)
{
	vec_t* vec;
	vec = vec_init(sizeof(char));

	vec_append(vec, "t");
	vec_append(vec, "test");
	vec_append(vec, "vlad");
	vec_append(vec, "tomi");

	vec_append(vec, "This wont appear!");
	vec_pop(vec);

	vec_append(vec, "This won't appear either!");
	vec_del(vec, 4, sizeof(char));

	size_t i;
	for (i = 0; i < vec_size(vec); i++)
	{
		printf("%s\n", (char*)vec_get(vec, i));
	}
	printf("Total length: %lu\n", i);

	vec_destroy(vec);
	return 0;
}
```
